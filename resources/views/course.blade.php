<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title></title>
    <style>
    </style>
</head>

<body>
    <x-navbar></x-navbar>
    <div class="container">
        <x-message></x-message>
        <div class="col-md-6">
            <div class="card mt-4">
                <div  class="card-header text-white bg-secondary">
                  Register Here
                </div>
                <div class="card-body">
                  <form action="{{route('course')}}" method="post" enctype="multipart/form-data">
                   @csrf
                  <div class="form-group mb-3">
                      <input type="text" name="title" id="title" class="form-control" placeholder="Course title">
                  </div>
                 
              <div class="form-group">
                <select name="category[]" class="form-select">
                <option selected disabled>course select</option>
                    <option value="Short">Short</option>
                    <option value="Long">Long</option>
                    <option value="Deploma">Deploma</option>
                </select>
            </div>
            <br>
            <label>Type &nbsp;:</label>
            <div class="form-check form-check-inline">
                <input type="radio" name="type" id="online" value="online" class="form-check-input">
                <label class="form-check-label">online</label>
            </div>
            <div class="form-check form-check-inline">
                <input type="radio" name="type" id="offline" value="offline"class="form-check-input">
                <label class="form-check-label">offline</label>
            </div><br>
            <label>Technology</label>
            <div class="form-check form-check-inline">
                <input type="checkbox" name="technology[]" value="HTML" class="form-check-input">
                <label class="form-check-label" >HTML</label>
            </div>
            <div class="form-check form-check-inline">
                <input type="checkbox" name="technology[]" value="Javascript"class="form-check-input">
                <label class="form-check-label" >JavaScript</label>
            </div>
            <div class="form-check form-check-inline">
                <input type="checkbox" name="technology[]" value="PHP"class="form-check-input">
                <label class="form-check-label" >PHP</label>
            </div>
                <br>
                <div class="form-group mb-3 mt-3">
                   
                    <input type="text" name="duration" placeholder="Course-duration" class="form-control">
                    
                </div>
                <div class="form-group">
                    <label>Start-date</label>
                    <input type="date" name="start_date" class="form-control">
                </div>
                <div class="form-group mb-3">
                    <label>Image</label>
                    <input type="file" name="image" class="form-control">
                </div>
                <div class="form-group mb-3">
                      <button type="submit" class="btn btn-block btn-secondary form-control">Register Now</button>
                 </div>
                
        </form>
    </div>
    </div>
    </div>
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
</body>

</html>