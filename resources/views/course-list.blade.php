<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>Document</title>
</head>

<body>

    <x-navbar></x-navbar>
    <div class="container" style="padding-top: 10px;">
        <div class="row">
            <div class="col-md-12">
                <div class="row mb-3">
                    <div class="col-md-6 ">
                        <x-error></x-error>
                    </div>
                    <div class="col-md-6">
                        <div class="btn-group" role="group" aria-label="Basic outlined example">
                            <a class="btn btn-secondary" href="{{ route('course-create')}}">Create</a>
                            <a class="btn btn-outline-secondary" href="{{ route('trash')}}">Trash</a>
                            <a class="btn btn-outline-secondary" href="{{ route('pdf')}}">PDF</a>
                            <button type="button" class="btn btn-outline-secondary">#</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <table class="table table-dark table-striped">
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Type</th>
                            <th>Technology</th>
                            <th>Duration</th>
                            <th>Start-date</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                        @foreach($course as $row)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td> {{ $row->title }}</td>
                            <td> {{ $row->category }}</td>
                            <td> {{ $row->type }}</td>
                            <td> {{ $row->technology }}</td>
                            <td> {{ $row->duration }}</td>
                            <td> {{ $row->start_date }}</td>
                            <td><img src="{{ asset('storage/course/'.$row->image)}}" width="70px" height="50px"></td>
                            <td><a href="{{ route('course-edit',$row->id) }}" class="btn btn-primary">Edit </a>
                                <form action="{{ route('course-destroy', $row->id) }}" method="post" style="display:inline">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button class="btn  btn-danger">Delete</button>
                                </form>
                            </td>

                        </tr>
                        @endforeach
                    </table>
                    {{$course->links()}}
                </div>
            </div>


            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>

</body>

</html>