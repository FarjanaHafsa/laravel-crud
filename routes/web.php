<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\CourseController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('welcome');
});


Route::get('/course', [CourseController::class, 'create'])->name('course-create');
Route::post('/course_store', [CourseController::class, 'store'])->name('course');
Route::get('/course_list', [CourseController::class, 'index'])->name('course-list');
Route::get('/course/{id}/edit', [CourseController::class, 'edit'])->name('course-edit');
Route::delete('/course/{id}/destroy', [CourseController::class, 'destroy'])->name('course-destroy');
Route::patch('/course/{id}', [CourseController::class, 'update'])->name('course_update');


//Route::resource('course', CourseController::class);

Route::get('/trash',[CourseController::class,'trash'])->name('trash');
Route::delete('/course/{id}/delete', [CourseController::class, 'delete'])->name('course-delete');
Route::get('/course/restore/{id}', [CourseController::class, 'restore'])->name('course-restore');
Route::get('generate-pdf', [CourseController::class, 'generatePDF'])->name('pdf');