<?php

namespace App\Http\Controllers;

use App\Models\Course;
use Illuminate\Http\Request;
use App\http\Requests\CategoryRequest;
use Barryvdh\DomPDF\Facade\Pdf;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $course = Course::latest()->paginate(5);
        return view('course-list', compact('course'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('course');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {

        $original = $request->file('image')->getClientOriginalName();
        $filename = time() . $original;
        $request->file('image')->move(storage_path('/app/public/course'), $filename);

        $category = $request['category'];
        $technology = $request['technology'];

        $request['technology'] = implode(',', $technology);
        $request['category'] = implode(',', $category);


        $data = [
            'title' => $request['title'],
            'category' => $request['category'],
            'type' => $request['type'],
            'technology' => $request['technology'],
            'duration' => $request['duration'],
            'start_date' => $request['start_date'],
            'image'     => $filename
        ];


        Course::create($data);

        //return redirect()->route('course-list');
        return redirect()->route('course-list')->withMessage('Inserted sucessfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        //
    }
    public function generatePDF()
    {
        $users = Course::all();

        $data = [
            'title' => 'Welcome to Course.com',
            'date' => date('m/d/Y'),
            'users' => $users
        ];

        $pdf = Pdf::loadView('pdf', $data);

        return $pdf->download('course.pdf');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course, $id)
    {
        $course = Course::find($id);
        return view('course-edit', Compact('course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, Course $course, $id)
    {

        $course = Course::find($id);
        $filename = '';
        if ($request->hasFile('image')) {

            $original = $request->file('image')->getClientOriginalName();
            $filename = time() . $original;
            $request->file('image')->move(storage_path('/app/public/course'), $filename);
        } else {
            $filename = $course->image;
        }


        $category = $request['category'];
        $technology = $request['technology'];

        $request['technology'] = implode(',', $technology);
        $request['category'] = implode(',', $category);


        $data = [
            'title' => $request['title'],
            'category' => $request['category'],
            'type' => $request['type'],
            'technology' => $request['technology'],
            'duration' => $request['duration'],
            'start_date' => $request['start_date'],
            'image' => $filename

        ];



        $course->update($data);

        return redirect()->route('course-list')->withMessage('Updated sucessfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course, $id)
    {
        $course = Course::find($id);
        $course->delete();
        return redirect()->route('course-list')->withMessage('Deleted sucessfully');
    }

    public function trash()
    {
        $course = Course::onlyTrashed()->get();
        return view('trash', compact('course'));
    }
    public function restore($id)
    {
        $course = Course::onlyTrashed()->find($id);
        $course->restore();

        return redirect()
            ->route('trash')
            ->withMessage('Successfully restored');
    }

    public function delete($id)
    {
        $category = Course::onlyTrashed()->find($id);
        $category->forceDelete();

        return redirect()
            ->route('trash')
            ->withMessage('Successfully deleted');
    }
}
