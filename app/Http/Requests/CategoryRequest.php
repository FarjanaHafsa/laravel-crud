<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {

        if ($this->getMethod() == 'POST') {
            $rules = [

                'title' => 'required|unique:courses|max:25|min:3',
                'category' => 'required',
                'type' => 'required',
                'technology' => 'required',
                'duration'  => 'required',
                'start_date' => 'required',
                'image'      => 'required'
            ];
        }
        if ($this->getMethod() == 'PATCH') {


            $rules = [
                'title' => 'required',
                'category' => 'required',
                'type' => 'required',
                'technology' => 'required',
                'duration'  => 'required',
                'start_date' => 'required'
            ];
        }
        return $rules;
    }
}
